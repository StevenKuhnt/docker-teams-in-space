FROM docker-java8
MAINTAINER David Jenkins "djenkins@atlassian.com"

# Install software
RUN add-apt-repository ppa:git-core/ppa -y && \
    apt-get update -y && \
    apt-get install -y \
        git \
        vim && \
    apt-get clean all && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir /root/.ssh/
COPY id_rsa /root/.ssh/id_rsa
RUN chmod 400 /root/.ssh/id_rsa && \
    touch /root/.ssh/known_hosts && \
    ssh-keyscan "bitbucket.org" >> /root/.ssh/known_hosts && \
    git clone git@bitbucket.org:StevenKuhnt/teams-in-space-demo.git /opt/atlassian && \
    echo "source /opt/atlassian/setup.sh" >> /root/.profile && \
    echo "source /opt/atlassian/setup.sh" >> /root/.bashrc
COPY ./boot/ /root/boot
RUN chmod +x /root/boot/entrypoint.sh && \
    chmod +x /root/boot/startup.sh    && \
    chmod +x /root/boot/*

ENTRYPOINT ["/root/boot/entrypoint.sh"]
