#!/bin/bash
echo "startup for full Teams In Space Docker Build"
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export JRE_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre/
export PATH=$PATH:/usr/lib/jvm/java-8-openjdk-amd64/bin:/usr/lib/jvm/java-8-openjdk-amd64/jre/bin
wget -O ~/v2.17.0.zip https://github.com/git/git/archive/v2.17.0.zip
cd ~/ && unzip v2.17.0.zip
cd ~/git-2.17.0
make configure
./configure --prefix=/usr
make prefix=/usr
make prefix=/usr install
git version
java -version
php -v
source /opt/atlassian/setup.sh && tis-install
yes | tis-reset && /bin/bash
cd /opt/atlassian