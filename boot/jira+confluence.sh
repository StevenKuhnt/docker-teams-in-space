#!/bin/bash
echo "startup for jira+confluence.sh file"
source /opt/atlassian/setup.sh && tis-install
cd /opt/atlassian/scripts
./movetopresent.sh
cd /opt/atlassian
start-crowd && start-jira && start-confluence
sleep 30
tis-reindex
echo "successfully completed startup for Jira and Confluence"
tis-check && /bin/bash