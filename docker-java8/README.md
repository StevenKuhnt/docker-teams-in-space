Docker Oracle Java
=========

This is a Dockerfile for starting a Oracle Java 8 on an Ubuntu container with [Java](https://www.java.com/en/) `8u31` installed. 

# Installation of docker-java8
	docker build -t docker-java8

Then run `cd ..` back to the home directory. This build of Java8 runs in side the main Docker build as a separated version of Java8 dependency.
